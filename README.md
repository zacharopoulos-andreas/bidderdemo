# What is this repository for? 
The project implements a bidding mechanism.

The bidder receives bid requests from an ad exchange and it responds back either with a bid or without one. You can see examples of both cases below.
The bid request from the ad exchange contains info that is needed by the bidder to perform its operation. Test are included.

# Requirements
Java: 1.8.x 

Maven: 3.6.3

# Steps to setup
## 1. Clone the application: 
1.1  git clone https://bitbucket.org/zacharopoulos-andreas/bidderdemo.git

	OR

1.2 navigate to https://bitbucket.org/zacharopoulos-andreas/bidderdemo/src/master/ and clone

## 2. Build and run using maven 
mvn clean 

mvn install

mvn spring-boot:run

The app will start running at http://localhost:8080.

# Test the application
Either run BidderApplicationTests or Import Bidder.postman_collection and run manual tests


# Explore Rest APIs
The app defines following CRUD APIs.
## Endpoints
#### POST /api/bid

Receive bid requests from an ad exchange and respond back either with a bid or without one

		Example with returning bid
			{
			  "id": "e7fe51ce4f6376876353ff0961c2cb0d",
			  "app": {
				"id": "e7fe51ce-4f63-7687-6353-ff0961c2cb0d",
				"name": "Morecast Weather"
			  },
			  "device": {
				"os": "Android",
				"geo": {
				  "country": "USA",
				  "lat": 0,
				  "lon": 0
				}
			  }
			}
			
		Example with no returning bid (Status.204)
			{
			  "id": "e7fe51ce4f6376876353ff0961c2cb0d",
			  "app": {
				"id": "e7fe51ce-4f63-7687-6353-ff0961c2cb0d",
				"name": "Morecast Weather"
			  },
			  "device": {
				"os": "Android",
				"geo": {
				  "country": "CYP",
				  "lat": 0,
				  "lon": 0
				}
			  }
			}


