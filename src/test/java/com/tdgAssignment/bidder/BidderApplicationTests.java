package com.tdgAssignment.bidder;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.tdgAssignment.bidder.model.BidRequest;
import com.tdgAssignment.bidder.model.BidResponse;
import com.tdgAssignment.bidder.rest.BidController;
import com.tdgAssignment.bidder.utils.JsonParser;

@SpringBootTest
class BidderApplicationTests {
	@Autowired
	private JsonParser parser;
	@Autowired
	BidController bidController;

	@Test
	public void shouldReturnMatchingCampaigns() {
		String shouldReturnMatchingCampaignsRequest = ("{\n" + "  \"id\": \"e7fe51ce4f6376876353ff0961c2cb0d\",\n"
				+ "  \"app\": {\n" + "    \"id\": \"e7fe51ce-4f63-7687-6353-ff0961c2cb0d\",\n"
				+ "    \"name\": \"Morecast Weather\"\n" + "  },\n" + "  \"device\": {\n" + "    \"os\":\"Android\",\n"
				+ "    \"geo\": {\n" + "      \"country\": \"USA\",\n" + "      \"lat\": 0,\n" + "      \"lon\": 0\n"
				+ "    }\n" + "  }\n" + "}");
		String shouldReturnMatchingCampaignsResponse = ("{\n" + "  \"id\": \"e7fe51ce4f6376876353ff0961c2cb0d\",\n"
				+ "  \"bid\": {\n" + "    \"campaignId\": \"5a3dce46\",\n" + "    \"price\": 1.23,\n"
				+ "    \"adm\": \"<a href=\\\"http://example.com/click/qbFCjzXR9rkf8qa4\\\"><img src=\\\"http://assets.example.com/ad_assets/files/000/000/002/original/banner_300_250.png\\\" height=\\\"250\\\" width=\\\"300\\\" alt=\\\"\\\"/></a><img src=\\\"http://example.com/win/qbFCjzXR9rkf8qa4\\\" height=\\\"1\\\" width=\\\"1\\\" alt=\\\"\\\"/>\\r\\n\"\n"
				+ "  }\n" + "}\n" + "");

		BidRequest request = parser.parseBidRequest(shouldReturnMatchingCampaignsRequest);
		BidResponse expectedResponse = parser.parseBidResponse(shouldReturnMatchingCampaignsResponse);
		ResponseEntity<BidResponse> actualResponse =  bidController.executeBidding(request);
		System.out.println((expectedResponse).equals(actualResponse.getBody()));
	}

	@Test
	public void shouldNotReturnMatchingCampaigns() {
		String shouldNotReturnMatchingCampaignsRequest = ("{\n"
				+ "  \"id\": \"e7fe51ce4f6376876353ff0961c2cb0d\",\n"
				+ "  \"app\": {\n"
				+ "    \"id\": \"e7fe51ce-4f63-7687-6353-ff0961c2cb0d\",\n"
				+ "    \"name\": \"Morecast Weather\"\n"
				+ "  },\n"
				+ "  \"device\": {\n"
				+ "    \"os\": \"Android\",\n"
				+ "    \"geo\": {\n"
				+ "      \"country\": \"CYP\",\n"
				+ "      \"lat\": 0,\n"
				+ "      \"lon\": 0\n"
				+ "    }\n"
				+ "  }\n"
				+ "}"); 

		BidRequest request = parser.parseBidRequest(shouldNotReturnMatchingCampaignsRequest);
		ResponseEntity<BidResponse> actualResponse =  bidController.executeBidding(request);
		System.out.println(actualResponse.getBody()==null);
	}

}
