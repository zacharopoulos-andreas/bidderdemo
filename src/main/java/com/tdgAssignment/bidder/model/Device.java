package com.tdgAssignment.bidder.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "os", "geo" })
public class Device {

	@JsonProperty("os")
	private Device.Os os;

	@JsonProperty("geo")
	private GeolocationInfo geo;

	public Device.Os getOs() {
		return os;
	}

	public void setOs(Device.Os os) {
		this.os = os;
	}

	public GeolocationInfo getGeo() {
		return geo;
	}

	public void setGeo(GeolocationInfo geo) {
		this.geo = geo;
	}

	public enum Os {

		ANDROID("Android"), 
		I_OS("iOS");

		private final String label;
		private final static Map<String, Device.Os> OSYSTEMS = new HashMap<String, Device.Os>();

		static {
			for (Device.Os c : values()) {
				OSYSTEMS.put(c.label, c);
			}
		}

		private Os(String label) {
			this.label = label;
		}

		@Override
		public String toString() {
			return this.label;
		}

		public String value() {
			return this.label;
		}

		@JsonCreator
		public static Device.Os fromValue(String value) {
			Device.Os constant = OSYSTEMS.get(value);
			if (constant == null) {
				throw new IllegalArgumentException(value);
			} else {
				return constant;
			}
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((geo == null) ? 0 : geo.hashCode());
		result = prime * result + ((os == null) ? 0 : os.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Device))
			return false;
		Device other = (Device) obj;
		if (geo == null) {
			if (other.geo != null)
				return false;
		} else if (!geo.equals(other.geo))
			return false;
		if (os != other.os)
			return false;
		return true;
	}
}