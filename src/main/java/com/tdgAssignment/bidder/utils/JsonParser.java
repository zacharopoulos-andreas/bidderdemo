package com.tdgAssignment.bidder.utils;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tdgAssignment.bidder.model.BidRequest;
import com.tdgAssignment.bidder.model.BidResponse;

@Component
public class JsonParser {

	public BidRequest parseBidRequest(String requestString) {
		Gson gsonRequest = new GsonBuilder().create(); 
		return gsonRequest.fromJson(requestString, BidRequest.class);
	}
	
	public BidResponse parseBidResponse (String responseString) {
		Gson gsonResponse = new GsonBuilder().create(); 
		return gsonResponse.fromJson(responseString, BidResponse.class);
	}
	
}
