package com.tdgAssignment.bidder.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tdgAssignment.bidder.model.BidRequest;
import com.tdgAssignment.bidder.model.BidResponse;
import com.tdgAssignment.bidder.service.BidderService;

@RestController
@RequestMapping("/api/bid")
public class BidController {
	
	@Autowired
	private BidderService bidderService;
	
	@PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BidResponse> executeBidding (@RequestBody BidRequest request) {
		return !(bidderService.executeBidding(request)==null)?  ResponseEntity.ok(bidderService.executeBidding(request)) : new ResponseEntity<BidResponse>(HttpStatus.NO_CONTENT) ;
    }
	
}
