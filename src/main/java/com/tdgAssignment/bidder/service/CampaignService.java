package com.tdgAssignment.bidder.service;

import com.tdgAssignment.bidder.model.Campaign;

public interface CampaignService {
	public Campaign[] retrieveCampaigns ();
}
