package com.tdgAssignment.bidder.service;

import com.tdgAssignment.bidder.model.BidRequest;
import com.tdgAssignment.bidder.model.BidResponse;

public interface BidderService {
	public BidResponse executeBidding (BidRequest request);
}
