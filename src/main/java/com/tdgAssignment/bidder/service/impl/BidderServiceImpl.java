package com.tdgAssignment.bidder.service.impl;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdgAssignment.bidder.model.Bid;
import com.tdgAssignment.bidder.model.BidRequest;
import com.tdgAssignment.bidder.model.BidResponse;
import com.tdgAssignment.bidder.model.Campaign;
import com.tdgAssignment.bidder.service.BidderService;
import com.tdgAssignment.bidder.service.CampaignService;

@Service
public class BidderServiceImpl implements BidderService {

	@Autowired
	CampaignService campaignService;
	
	@Override
	public BidResponse executeBidding(BidRequest request) {
		
		Campaign[] campaigns = campaignService.retrieveCampaigns();		
		List<Campaign> matchingCampaigns=Arrays.asList(campaigns).stream()
				.filter(c -> c.getTargetedCountries().contains(request.getDevice().getGeo().getCountry()))
				.collect(Collectors.toList());
		
		if (!matchingCampaigns.isEmpty()) {			
			Campaign campaign = matchingCampaigns.stream().max(Comparator.comparingDouble(Campaign::getPrice)).get();			
			BidResponse bidResponse = new BidResponse();
			bidResponse.setBid(mapBid(campaign));
			bidResponse.setId(request.getId());		
			return bidResponse;	
		} 
		return null;
	}
	
	private Bid mapBid (Campaign campaign) {
		Bid bid = new Bid();
		bid.setAdm(campaign.getAdm());
		bid.setCampaignId(campaign.getId());
		bid.setPrice(campaign.getPrice());
		return bid;		
	}
}
