package com.tdgAssignment.bidder.service.impl;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tdgAssignment.bidder.model.Campaign;
import com.tdgAssignment.bidder.service.CampaignService;
import com.tdgAssignment.bidder.utils.Utilities;

@Service
public class CampaignServiceImpl implements CampaignService {

	
	public Campaign[] retrieveCampaigns() {
		Gson gsonCampaigns = new GsonBuilder().create();
		return gsonCampaigns.fromJson(Utilities.campaignString, Campaign[].class);
	}

}
